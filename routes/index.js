var express = require('express');
var router = express.Router();

var xray = require('x-ray')();
//var NUFORC_ROOT_URL = 'http://www.nuforc.org/webreports/ndxevent.html';

// GET /sightings
// Required query params: year, month
router.get('/sightings', function(req, res, next) {
  res.setHeader('Content-Type', 'application/json; charset=utf-8');

  if (req.query.year == null) {
    res.status(400).send('You must specify a year, such as, 2015.');
  } else if (req.query.month == null) {
    res.status(400).send('You must specify a month, such as, 04 for April.');
  }

  var url = `http://www.nuforc.org/webreports/ndxe${req.query.year}${req.query.month}.html`;

  var stream = xray(url, 'tr', [{
    time: 'td:first-child',
    city: 'td:nth-child(2)',
    state: 'td:nth-child(3)',
    shape: 'td:nth-child(4)',
    duration: 'td:nth-child(5)',
    summary: 'td:nth-child(6)'
  }]).write();

  stream.pipe(res);

  stream.on('data', function (data) {
    res.write(data);
  });

  stream.on('end', function () {
    res.end();
  });
});

module.exports = router;
